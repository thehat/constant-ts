/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(1));


/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";
	var Constant = (function () {
	    function Constant(original) {
	        if (typeof original === "undefined")
	            throw new Error("Constant was initialized with undefined value.");
	        if (original == null)
	            throw new Error("Constant was initialized with null.");
	        if (this.isPrimitive(original))
	            throw new Error("Constant was initialized with primitive.");
	        this.copy = this.clone(original);
	    }
	    Constant.prototype.get = function (getter) {
	        var result;
	        try {
	            result = getter(this.copy);
	        }
	        catch (e) { }
	        if (this.isPrimitive(result)) {
	            return result;
	        }
	        if (!result)
	            return null;
	        return new Constant(result);
	    };
	    Constant.prototype.clone = function (target) {
	        var assing = Object['assign'];
	        var clone = typeof assing != "undefined"
	            ? assing({}, target)
	            : JSON.parse(JSON.stringify(target));
	        return clone;
	    };
	    Constant.prototype.isPrimitive = function (target) {
	        return typeof target === "number"
	            || typeof target === "string"
	            || typeof target === "boolean"
	            || typeof target === "function";
	    };
	    return Constant;
	}());
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Constant;


/***/ }
/******/ ]);
"use strict";
var Constant = (function () {
    function Constant(original) {
        if (typeof original === "undefined")
            throw new Error("Constant was initialized with undefined value.");
        if (original == null)
            throw new Error("Constant was initialized with null.");
        if (this.isPrimitive(original))
            throw new Error("Constant was initialized with primitive.");
        this.copy = this.clone(original);
    }
    Constant.prototype.get = function (getter) {
        var result;
        try {
            result = getter(this.copy);
        }
        catch (e) { }
        if (this.isPrimitive(result)) {
            return result;
        }
        if (!result)
            return null;
        return new Constant(result);
    };
    Constant.prototype.clone = function (target) {
        var assing = Object['assign'];
        var clone = typeof assing != "undefined"
            ? assing({}, target)
            : JSON.parse(JSON.stringify(target));
        return clone;
    };
    Constant.prototype.isPrimitive = function (target) {
        return typeof target === "number"
            || typeof target === "string"
            || typeof target === "boolean"
            || typeof target === "function";
    };
    return Constant;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Constant;

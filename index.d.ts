export default class Constant<T> {
    private copy;
    constructor(original: T);
    get<V extends number | string | boolean | Function>(getter: (original: T) => V): V;
    get<V>(getter: (original: T) => V): Constant<V>;
    private clone<V>(target);
    private isPrimitive(target);
}

Constant
========

[![build status](https://gitlab.com/thehat/constant-ts/badges/master/build.svg)](https://gitlab.com/thehat/constant-ts/commits/master)

A class that represents immutable data.

`Constant<T>` objects contain inner copy of original model of class `T` and provide read-only access to fields.

Usage
-----

For class
```typescript
class TreeNode
{
    parent: TreeNode;
    value: string;
}
```

Lets create some data structure:
```typescript
let root = new TreeNode();
root.value = "root";

let child = new TreeNode();
child.parent = root;
child.value = "child";
```

Probably it should be stored in some immutable store (like Redux one :wink:).

To provide new state with a garant of lack of mutation on original model we use `Constant<TreeNode>`:

```typescript
let childConstant = new Constant(child);
...

console.log(childConstant); // is Constant<TreeNode> for child

let childValue = childConstant.get(c => c.value);
console.log(childValue); // "child"

let rootConstant = childConstant.get(c => c.root);
let rootValue = rootConstant.get(c => c.value);
console.log(rootValue); // "root"

```

export default class Constant<T>
{
    private copy: T;

    constructor(original: T)
    {
        if (typeof original === "undefined")
            throw new Error("Constant was initialized with undefined value.")
        if (original == null)
            throw new Error("Constant was initialized with null.")
        if (this.isPrimitive(original))
            throw new Error("Constant was initialized with primitive.")

        this.copy = this.clone(original);
    }

    public get<V extends number | string | boolean | Function>(getter: (original: T) => V) : V
    public get<V>(getter: (original: T) => V) : Constant<V>
    public get(getter: (original: T) => any) : any
    {
        let result: any;
        try
        {
            result = getter(this.copy);
        }
        catch (e) { }

        if (this.isPrimitive(result))
        {
            return result;
        }

        if (!result)
            return null;

        return new Constant(result);
    }

    private clone<V>(target: V): V
    {
        let assing: (obj: any, original: V) => V = (Object as any)['assign'];
        let clone = typeof assing != "undefined"
            ? assing({}, target)
            : JSON.parse(JSON.stringify(target)) as V;

        return clone;
    }

    private isPrimitive(target: any): boolean
    {
        return typeof target === "number"
            || typeof target === "string"
            || typeof target === "boolean"
            || typeof target === "function";
    }
}

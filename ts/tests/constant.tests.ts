/// <reference path="../../typings/tsd.d.ts" />

import Constant from "../constant/constant";
import "../../lib/jasmine";

class A
{
    simpleString: string;
    simpleNumber: number;
    thatSimple: boolean;
    nothing: B;
    nothingString: string;
    testObject: B;
    testFunc: Function;
}

class B 
{
    public someField: string;
}

let original: A = {
    simpleString: "abbacabba",
    simpleNumber: 123.456,
    thatSimple: true,

    nothing: null as B,
    nothingString: null as string,

    testObject: {
        someField: "12312"
    } as B,

    testFunc: () => {}
};

describe("A constant", () =>
{
    it("should throw an error when initialized by null", () =>
    {
        let constantMaker = () => new Constant(null);

        expect(constantMaker).toThrowError("Constant was initialized with null.");
    });

    it("should throw an error when initialized by undefined", () =>
    {
        var undef: any;
        let constantMaker = () => new Constant(undef);

        expect(constantMaker)
            .toThrowError("Constant was initialized with undefined value.");
    });

    it("shoud not be null when initialized by {}", () =>
    {
        let constant = new Constant({});

        expect(constant).not.toBeNull();
    });

    it("shoud not be initialized with primitive", () =>
    {
        let numberConstantMaker = () => new Constant(1.2);
        let stringConstantMaker = () => new Constant("abba");
        let booleanConstantMaker = () => new Constant(true);
        let funcConstantMaker = () => new Constant(() => {});

        expect(numberConstantMaker)
            .toThrowError("Constant was initialized with primitive.");
        expect(stringConstantMaker)
            .toThrowError("Constant was initialized with primitive.");
        expect(booleanConstantMaker)
            .toThrowError("Constant was initialized with primitive.");
        expect(funcConstantMaker)
            .toThrowError("Constant was initialized with primitive.");
    });
});

describe("A primitive constant", () =>
{
    it("shoud return string when targeted a string", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.simpleString);

        expect(result).toEqual(jasmine.any(String));
        expect(result).toBe(original.simpleString);
    });

    it("shoud return number when targeted a number", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.simpleNumber);

        expect(result).toEqual(jasmine.any(Number));
        expect(result).toBe(original.simpleNumber);
    });

    it("shoud return boolean when targeted a boolean", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.thatSimple);

        expect(result).toEqual(jasmine.any(Boolean));
        expect(result).toBe(original.thatSimple);
    });

    it("shoud return null when targeted a null string", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.nothingString);

        expect(result).toBeNull();
    });
});

describe("A complex constant", () =>
{
    it("shoud return Constant<TestClass> when targeted TestClass", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.testObject);
        let value = result.get(r => r.someField);

        let expectedResult = new Constant(original.testObject);

        expect(result).toEqual(jasmine.any(Constant));
        expect(result).toEqual(expectedResult);

        expect(value).toBe(original.testObject.someField);
    });

    it("shoud return null when targeted a null", () =>
    {
        let constant = new Constant(original);
        let result = constant.get(c => c.nothing);

        expect(result).toBeNull();
    });

    it("shoud not change underlying value", () =>
    {
        type TestType = {
            some: string
        };

        let value: TestType = {
            some: "thing"
        };
        let constant = new Constant(value);

        let mutator = (x: TestType) => {
            x.some = "other thing"
        }; 

        let result = constant.get(mutator);

        expect(value.some).toBe("thing");
    });

    it("shoud not throw error on null propagation", () =>
    {
        let constant = new Constant(original);
        let fieldGetter = () => constant.get(c => c.nothing.someField);

        expect(fieldGetter).not.toThrowError();

        let someField = fieldGetter();
        expect(someField).toBeNull();
    });
});
